# Welcome Week 2024

Brief explanation of Welcome Week team-build task

## Getting to know each other and some tech...

You will create a small team, find out about each other's skills and interests, and produce a quick and dirty page on our Gitlab to pitch those skills and interests.    

In your browser, go to https://csgitlab.reading.ac.uk and log in using your university username (e.g. xx123456) and password.

[* Note if https://csgitlab.reading.ac.uk is not working for some reason, you can use https://gitlab.act.reading.ac.uk instead]

# Instructions at https://csgitlab.reading.ac.uk/patparslow/welcome-week-2024 

This is GitLab - you will be using it via this web interface, and via the command line, in many of your modules during your degree.  It provides a way of keeping a backup of your work, and collaborating with other people.

Create a new project, using the "New project" button.  Create a "Blank project", using the button.

Call your project "Welcome Week", and put in a brief description, such as "Practice project in team building session".

Leave the other fields on the form as they are, and click the button to complete the creation process.


## Find a team
Now - find some people to work with!  For this to work best, you will want to add at least 3 other people from the session you are in to your project.  To do that, you will need a couple of things:

1. To find out what their usernames are, and
2. For them to have logged in to https://csgitlab.reading.ac.uk 

Then you can click on Project information (on the left hand menu), and then "Members".  You can use the "Invite Members" button from there to issue invitations to people to join your project.  I'd suggest you might want to add me, @patparslow, too.

You can leave their role as 'Guest'.

## Create

Two tasks:

1. Now - get talking!  Find out about each other's skills and interests and work together to produce a brief outline (e.g. look at company websites - a paragraph each).  
   You aren't expected to be experts in programming or other Computer Science areas; you can include hobbies, gaming, sports, natural history, environmental issues, favourite book genres, people skills, global social awareness (and so on).

2. Discuss the role of LLM chatbots in education - opportunities, risks, how to use them sensibly



For information on creating a document on GitLab, _formatting text_ and **general usage**, first of all try exploring the site (!) and look for the help files; discuss with your team, ask us, or even search online!  In higher education, one of the skills you develop is seeking and evaluating information to make you **independent learners**.
